# Итоговый проект курса «DevOps-инженер. Основы»

## Статус

Рекомендован к использованию

## Задача

Развернуть сервис, мониторинг сервиса и систему логирования в облачной среде Yandex Cloud

## Решение 

### Топология с локальным Gitlab Runner

![Topology Local Runner](png/skillbox_project_local_runner.png?raw=true "Topology Local Runner")

### Топология с удалённым Gitlab Runner

![Topology Remote Runner](png/skillbox_project_remote_runner.png?raw=true "Topology Remote Runner")

### Запуск и останов проекта

1) Проект запускается либо на локальном либо на облачном сервере Centos 8. В качестве образа для запуска пайплайна GitLab в контейнере используется модифицированный образ gitlab-terraform с добавленными Ansible и зеркалами Яндекс для удобства

2) Измените переменную reg_token в файле gitlab-runner/vars.yml на соответствующую из раздела Gitlab - Project - Settings - CI/CD - Runners - Specific runners

3) Запустите развертывание и настройку gitlab-runner. Если планируется развернуть gitlab-runner на локальном сервере, то запустите на выполнение скрипт:
```
sh rollout.sh local
```
, который проинсталлирует необходимое ПО и зарегистрирует локальный Gitlab-Runner с экзекьютором docker на сервере CI/CD gitlab.com. Если планируется запустить проект на удаленном сервере, то запустите на выполнение скрипт:
```
sh rollout.sh remote
```
, который проинсталлирует необходимое ПО и зарегистрирует удаленный (в Yandex-облаке) Gitlab-Runner с экзекьютором docker на сервере gitlab.com

4) Зайдите на локальный или удаленный сервер и склонируйте репозиторий
```
git clone https://gitlab.com/group11618/sb_project_t5.git
```

5) Во вновь созданном проекте укажите две переменные в разделе Gitlab - Project - Settings - CI/CD Variables: ID_RSA (ключ SSH для подключения к удаленной инфраструктуре) и YANDEX_OAUTH (OAuth-токен для управления облаком Yandex)

6) В директориях main/develop переименуйте файлы с расширением dist, убрав дополнительное расширение dist и указав нужные параметры

7) Инициализируйте локальный git-репозиторий

8) Отправьте проект в gitlab: 
```
git add .; git commit -m "commit descrption"; git push -uf origin main;
```

9) Дождитесь окончания работы джоб validate и plan. Вручную запустите джобу apply

10) Для проверки перейдите по адресу: http://akimov-petr.mooo.com:8080 (ветка develop http://lb-akimov.mooo.com:8080)

11) Для удаления проекта вручную запустите джобу destroy

### Мониторинг

Минимально необходимый пакет мониторинга уже включен в проект и не требует настройки. 
Мониторинг доступен по адресу: http://monitor-sb.duckdns.org:3000 (ветка develop http://grafana-sb.duckdns.org:3000 
логин/пароль по умолчанию admin/admin). На домашней странице Grafana выберите дашборд Prometheus Blackbox Exporter

![Monitoring](png/screen_04.png?raw=true "Monitoring")

### Рассылка уведомлений

Уведомления интегрированы в мессенджер Telegram. В минимально необходимый пакет уведомлений уже включены такие как:
- код ответа веб-сервера не равен 200
- опрос сервиса превысил таймаут, установленный равным 3 сек.
- заканчивается место на диске
- узел не доступен

Для получения уведомлений на своем гаджете в приложении Telegram перейдите по ссылке https://t.me/MiddleManBot и введите 
/start, чтобы получить токен приемника. Отредактируйте файл alertmanager.yaml, указав в настройках приемника токен:

```
receivers:
- name: 'telepush'
  webhook_configs:
  - url: 'https://telepush.dev/api/inlets/alertmanager/3d9efa' #URL с токеном после последнего слеша
```

![Alerting](png/alerting3.png?raw=true "Alerting")

### Система управления логами

Поскольку в системе уже присутствует мониторинг, основанный на стэке PLG (Prometheus – Grafana – Loki), то естественным и логичным решением было бы использовать систему логирования
Loki. Система логирования доступна по адресу: http://monitor-sb.duckdns.org:3000 (ветка develop http://grafana-sb.duckdns.org:3000
логин/пароль по умолчанию admin/admin). На домашней странице Grafana выберите дашборд Logs/App:

![Logging](png/logging_2.png?raw=true "Logging")

### Структура каталогов
```
├── gitlab-runner
│   ... (код для развертывания Gitlab-Runner)
├── README.md
├── rollout.sh (развертывание, настройка и регистрация Gitlab-Runner)
├── png
│   ... (скриншоты, топологии, рисунки)
├── src
│   ... (конфиги для сервиса, мониторинга и системы сбора логов)
├── develop
│   ... (IaC для развертывания проекта в ветке develop)
└── main
    ... (IaC для развертывания проекта в ветке main)
```

## Результаты

При развертывании проекта сервис, мониторинг, система логирования, вся необходимая инфраструктура и её настройка запускаются автоматически в параллельном режиме.