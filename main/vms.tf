##### VMs Part #####

resource "yandex_compute_instance" "monitor" {

  name        = "monitor"
  platform_id = "standard-v2"

  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }

  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      size = 10
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }

  scheduling_policy {
    preemptible = true
  }
}

resource "yandex_compute_instance" "ubuntu18" {
  name        = "ubuntu18"
  platform_id = "standard-v2"
  #  zone        = "ru-central1-a"

  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }

  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      size     = 10
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }

  scheduling_policy {
    preemptible = true
  }
}

##### ALB Target Group #####

resource "yandex_alb_target_group" "alb-tg" {
  name = "alb-tg"

  target {
    subnet_id  = yandex_vpc_subnet.subnet-1.id
    ip_address = yandex_compute_instance.ubuntu18.network_interface.0.ip_address
  }
}

##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[ubuntu18]
${yandex_compute_instance.ubuntu18.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

[monitor]
${yandex_compute_instance.monitor.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

EOF
  filename = "${path.module}/inventory"
}

##### Provisioning #####

resource "null_resource" "ubuntu18" {
  depends_on = [yandex_compute_instance.ubuntu18, local_file.inventory]

  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.ubuntu18.network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ansible.key pb.yml"
  }
}

##### Provisioning2 #####

resource "null_resource" "monitor" {
  depends_on = [yandex_compute_instance.monitor, local_file.inventory]

  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.monitor.network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ansible.key monitoring.yml"
  }
}
