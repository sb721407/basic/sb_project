##### Variables #####

locals {
  local_data = jsondecode(file("${path.module}/config.json"))
}

##### General #####

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
    backend "http" {}

  #  backend "s3" {
  #    endpoint   = "storage.yandexcloud.net"
  #    bucket     = "dev-bucket1976"
  #    region     = "ru-central1"
  #    key        = "test.tfstate"
  #    access_key = "YCAJEC2WEQ29IChKQlH4MoB24"
  #    secret_key = "YCNYnc-9e31rPzcQ4IAc8n1Pn51GxiAhD9fHH-OR"

  #    skip_region_validation      = true
  #    skip_credentials_validation = true
  #  }


}

variable "YANDEX_OAUTH" {
  type = string
}

provider "yandex" {
  #  token = file(local.local_data.OAUTH_FILE)
  #  service_account_key_file = local.local_data.KEY_FILE
  token     = var.YANDEX_OAUTH
  cloud_id  = local.local_data.CLOUD_ID
  folder_id = local.local_data.FOLDER_ID
  zone      = local.local_data.ZONE
}

##### Net & Subnet definition #####

resource "yandex_vpc_network" "network-2" {
  name = "network-2"
}

resource "yandex_vpc_subnet" "subnet-2" {
  name           = "subnet-2"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-2.id
  v4_cidr_blocks = ["192.168.2.0/24"]
}

